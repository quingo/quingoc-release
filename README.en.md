# Quingoc-release

#### Description
This repository is used to hold releases of the MLIR-based Quingo compiler before the MLIR compiler gets open source.

Please go to this page to view its releases:
https://gitee.com/quingo/quingoc-release/releases