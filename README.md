# Quingoc-release

#### 介绍
在 MLIR 编译器开源之前，此存储库用于保存基于 MLIR 的 Quingo 编译器的版本。

可在该页面查看各发行版:
https://gitee.com/quingo/quingoc-release/releases